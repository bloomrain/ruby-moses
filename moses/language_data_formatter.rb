class Moses
  class LanguageDataFormatter
    attr_reader :truecase_file_path, :config

    def initialize(config, language_params)
      @config = config
      @corpora_base_name = config.corpora_base_name
      @language = language_params.fetch('language')
      @file_path = language_params.fetch('file_path')

      @tokenized_file_path = "#{config.tokenized_file_base_path}.#{language}"
      @truecase_model_path = "#{config.truecase_model_base_path}.#{language}"
      @truecase_file_path = "#{config.truecast_file_base_path}.#{language}"
    end

    def call
      tokenize
      train_truecaster
      truecast
    end

    private

    attr_reader :file_path, :language, :corpora_base_name, :tokenized_file_path,
      :truecase_model_path

    def tokenize
      GeneratedFile.new(
        tokenized_file_path,
        command: "
          ./#{Moses::MOSES_PATH}/scripts/tokenizer/tokenizer.perl -l #{language} \
            < #{config.corpus_path}/#{file_path}    \
            > #{tokenized_file_path}
        "
      ).create
    end

    def train_truecaster
      GeneratedFile.new(
        truecase_model_path,
        command: "
          ./#{Moses::MOSES_PATH}/scripts/recaser/train-truecaser.perl \
            --corpus #{tokenized_file_path} \
            --model #{truecase_model_path}
        "
      ).create
    end

    def truecast
      GeneratedFile.new(
        truecase_file_path,
        command: "
         ./#{Moses::MOSES_PATH}/scripts/recaser/truecase.perl \
           --model #{truecase_model_path}         \
           < #{tokenized_file_path} \
           > #{truecase_file_path}
        "
      ).create
    end
  end
end
