class Moses
  module Tuning
    class Config < Moses::Config
      attr_reader :training_config

      delegate :download_test_data?, :config_label, to: :training_config

      def initialize(training_config)
        @training_config = training_config
      end

      def params
        @params ||= \
          if download_test_data?
            training_config.params.fetch('tuning')
          else
            {
              'corpora_base_name' => training_config.corpora_base_name,
              'source' => {
                'language' => training_config.source_config.fetch('language'),
                'file_path' => "#{training_config.source_config.fetch('file_path')}.test"
              },
              'target' => {
                'language' => training_config.target_config.fetch('language'),
                'file_path' => "#{training_config.target_config.fetch('file_path')}.test"
              }
            }
          end
      end
    end
  end
end
