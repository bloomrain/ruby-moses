class Moses
  class TuningDataFormatter < DataFormatter
    def call
      download if config.download_test_data?
      source_data_formatter.call
      target_data_formatter.call
      clean_long_sentences
    end
  end
end
