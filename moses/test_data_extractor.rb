class Moses
  class TestDataExtractor
    attr_reader :config, :source_file_path, :target_file_path

    def initialize(config)
      @config = config
      @source_file_path = "#{config.corpus_path}/#{config.source_config.fetch('file_path')}"
      @target_file_path = "#{config.corpus_path}/#{config.target_config.fetch('file_path')}"
    end

    def call
      split_file(source_file_path, source_lines)
      split_file(target_file_path, target_lines)
    end

    private

    def split_file(file_path, lines)
      return if test_file_generated_for?(file_path)

      testing_lines, training_lines = lines.partition.with_index do |_line, i|
        testing_line_indeces.include?(i)
      end

      FileUtils.mv(file_path, "#{file_path}.original")
      File.write(file_path, training_lines.join("\n"))
      File.write("#{file_path}.testing", testing_lines.join("\n"))
    end

    def test_file_generated_for?(file_path)
      File.exist?("#{file_path}.testing")
    end

    def source_lines
      @source_lines ||= read_lines(source_file_path)
    end

    def target_lines
      @target_lines ||= read_lines(target_file_path)
    end

    def testing_line_indeces
      @lines_to_pick ||= begin
        test_lines_count = source_lines.count * config.test_data_percents / 100
        lines_to_pick = Set.new
        while lines_to_pick.size != test_lines_count
          lines_to_pick << random_generator.rand(test_lines_count)
        end
        lines_to_pick
      end
    end

    def read_lines(file_path)
      File.read(file_path).split("\n").tap do |lines|
        lines.select!.with_index { |_, i| config.max_lines_to_use && i < config.max_lines_to_use }
      end
    end

    def random_generator
      @random_generator ||= Random.new(12345)
    end
  end
end
