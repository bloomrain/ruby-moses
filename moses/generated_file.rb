class Moses
  class GeneratedFile
    attr_reader :new_file_path, :command_runner

    def initialize(new_file_path, command:)
      @new_file_path = new_file_path
      @command_runner = CommandRunner.new(command, title: "Generating file #{new_file_path.inspect}")
    end

    def create
      if exist?
        Logger.info("[SKIP] file exist at: #{new_file_path}. Skipped command: #{command_runner.short_command}")
        return true
      end

      command_runner.call
    end

    def exist?
      File.exist?(new_file_path)
    end
  end
end
