class Moses
  class Tuner
    require_relative 'tuning/tuning_data_formatter'

    attr_reader :config

    delegate :tuning_config, :translation_system_path, to: :config

    def initialize(config)
      @config = config
    end

    def call
      TuningDataFormatter.new(tuning_config).call
      tune
      binarize_phrase_table
      binarize_reordering_table
    end

    private

    def tune
      GeneratedFile.new(
        "#{translation_system_path}/mert-work/moses.ini",
        command: "
          cd #{translation_system_path} && \
          #{Dir.pwd}/#{Moses::MOSES_PATH}/scripts/training/mert-moses.pl \
            #{Dir.pwd}/#{tuning_config.truecast_file_base_path}.#{tuning_config.source_language} \
            #{Dir.pwd}/#{tuning_config.truecast_file_base_path}.#{tuning_config.target_language} \
            #{Dir.pwd}/#{Moses::MOSES_PATH}/bin/moses #{Dir.pwd}/#{config.trained_model_config_path} \
            --mertdir #{Dir.pwd}/#{Moses::MOSES_PATH}/bin/ \
            --decoder-flags=\"-threads #{config.cores}\" \
        "
      ).create
    end

    def binarize_phrase_table
      GeneratedFile.new(
        config.binarized_phrase_table_path,
        command: "
          #{Moses::MOSES_PATH}/bin/processPhraseTableMin \
            -in #{config.phrase_table_path} -nscores 4 \
            -out #{config.binarized_phrase_table_path}
        "
      ).create
    end

    def binarize_reordering_table
      GeneratedFile.new(
        config.binarized_reordering_table_path,
        command: "
          #{Moses::MOSES_PATH}/bin/processLexicalTableMin \
            -in #{config.reordering_table_path} \
            -out #{config.binarized_reordering_table_path}
        "
      ).create
    end
  end
end
