require 'yaml'

require_relative 'language_data_formatter'
require_relative 'test_data_extractor'

class Moses
  class DataFormatter
    attr_reader :config

    delegate :source_config, :target_config, :corpora_download_url,
             :corpora_base_name, :truecast_base_file_path, to: :config

    def initialize(config)
      @config = config
    end

    def call
      download
      extract_testing_data unless config.download_test_data?
      source_data_formatter.call
      target_data_formatter.call
      clean_long_sentences
    end

    def download
      DataFetcher.new(config).call
    end

    def extract_testing_data
      TestDataExtractor.new(config).call
    end

    def source_data_formatter
      @source_data_formatter ||= LanguageDataFormatter.new(config, source_config)
    end

    def target_data_formatter
      @target_data_formatter ||= LanguageDataFormatter.new(config, target_config)
    end

    def clean_long_sentences
      GeneratedFile.new(
        "#{config.cleaned_corpora_file_path}.#{config.source_language}",
        command: "
          ./#{Moses::MOSES_PATH}/scripts/training/clean-corpus-n.perl \
           #{config.truecast_file_base_path} #{config.source_language} #{config.target_language} \
           #{config.cleaned_corpora_file_path} 1 80
        "
      ).create
    end

  end
end
