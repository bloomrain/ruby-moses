require_relative 'logger'

class Moses
  class CommandRunner
    def self.run(command, title: nil)
      runner = new(command, title: title).tap(&:call)
      runner.success? ? self : raise(StandardError.new('Command failed'))
    end

    attr_reader :command, :status, :title

    def initialize(command, title: nil)
      @command = command
      @status = :not_started
      @title = title || short_command
    end

    def call
      Logger.info("[START] #{title}")
      @status = system(command) ? :success : :failed

      if success?
        Logger.info("[END:success] #{title}")
      else
        Logger.info("[END:FAILED] #{short_command}")
        raise 'Something went wrong :('
      end
    end

    def success?
      @status == :success
    end

    def short_command
      command.gsub("\\\n", ' ').gsub("\n", '').gsub(/  +/, ' ')
    end
  end
end
