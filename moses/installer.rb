class Moses
  class Installer
    require 'fileutils'

    attr_reader :config

    def initialize(config)
      @config = config
    end

    def call
      install_moses unless moses_installed?
      install_giza unless giza_installed?
      create_directories if config.exist?
    end

    def all_installed?
      moses_installed? && giza_installed?
    end

    private

    def install_moses
      CommandRunner.run("
        sudo apt-get install build-essential git-core pkg-config automake libtool wget zlib1g-dev python-dev libbz2-dev && \
        git clone https://github.com/moses-smt/mosesdecoder.git #{Moses::MOSES_PATH} && \
        cd #{Moses::MOSES_PATH} && \
        make -f contrib/Makefiles/install-dependencies.gmake && \
        ./compile.sh --prefix=#{Dir.pwd}/#{Moses::MOSES_PATH} --install-scripts
      ")
    end

    def moses_installed?
      File.directory?('./executables/mosesdecoder')
    end

    def install_giza
      CommandRunner.run("
        git clone https://github.com/moses-smt/giza-pp.git #{config.giza_pp_path} && \
        cd #{config.giza_pp_path} && \
        make
      ")
      copy_giza_files unless giza_files_copied?
    end

    def copy_giza_files
      CommandRunner.run("
        cd #{Moses::MOSES_PATH} && \
        mkdir -p tools && \
        cp #{Dir.pwd}/#{config.giza_pp_path}/GIZA++-v2/GIZA++ #{Dir.pwd}/#{config.giza_pp_path}/GIZA++-v2/snt2cooc.out \
           #{Dir.pwd}/#{config.giza_pp_path}/mkcls-v2/mkcls tools
      ")
    end

    def giza_files_copied?
      File.directory?("./#{Moses::MOSES_PATH}/tools")
    end

    def giza_installed?
      File.directory?("./#{config.giza_pp_path}")
    end

    def create_directories
      FileUtils.mkdir_p(config.corpus_path)
      FileUtils.mkdir_p(config.language_model_path)
      FileUtils.mkdir_p(config.training_path)
      FileUtils.mkdir_p(config.tuning_config.corpus_path)
    end
  end
end
