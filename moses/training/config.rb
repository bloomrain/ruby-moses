class Moses
  module Training
    require_relative '../config'
    require_relative '../tuning/config'

    require 'yaml'
    require 'concurrent'

    class Config < Moses::Config
      attr_reader :config_label

      def initialize(config_label)
        @config_label = config_label
      end

      def cores
        @cores ||= Concurrent.processor_count
      end

      def tuning_config
        @tuning_config ||= Tuning::Config.new(self)
      end

      def giza_pp_path
        'executables/giza-pp'
      end

      def trained_model_config_path
        "#{training_path}/model/moses.ini"
      end

      def training_path
        "#{translation_system_path}/train"
      end

      def language_model_path
        "#{data_root_path}/language_model"
      end

      def prebinarized_language_model_base_path
        "#{language_model_path}/#{corpora_base_name}.arpa"
      end

      def binarized_language_model_base_path
        "#{language_model_path}/#{corpora_base_name}.blm"
      end

      def phrase_table_path
        "#{training_path}/model/phrase-table.gz"
      end

      def binarized_phrase_table_path
        "#{language_model_path}/phrase-table"
      end

      def reordering_table_path
        "#{training_path}/model/reordering-table.wbe-msd-bidirectional-fe.gz"
      end

      def binarized_reordering_table_path
        "#{language_model_path}/reordering_table"
      end

      def translation_system_path
        "#{data_root_path}/translation_system"
      end

      %i[
        dev_corpora_download_url
        test_data_percents
      ].each do |attribute|
        define_method(attribute) do
          params.fetch(attribute.to_s)
        end
      end

      def download_test_data?
        !params.key?('test_data_percents')
      end

      private

      def root_params
        YAML.load_file('moses/data_config.yml')
      end

      def params
        @params ||= root_params.fetch(config_label)
      end
    end
  end
end
