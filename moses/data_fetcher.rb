class Moses
  class DataFetcher
    attr_reader :url, :save_to

    def initialize(config)
      @url = config.corpora_download_url
      @save_to = config.corpus_path
    end

    def call
      return if downloaded_file.exist?
      downloaded_file.create
      extract
    end

    private

    def file_name
      url.split('/').last
    end

    def file_path
      "#{save_to}/#{file_name}"
    end

    def downloaded_file
      @downloaded_file ||= GeneratedFile.new(
        file_path,
        command: "cd #{save_to} && wget #{url}"
      )
    end

    def extract
      CommandRunner.run("cd #{save_to} && tar zxvf #{file_name}", title: "Extracting #{file_name.inspect}")
    end
  end
end
