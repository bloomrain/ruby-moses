class Moses
  class Config
    require 'yaml'
    require 'concurrent'

    def config_label
      raise NotImplementedError
    end

    def cores
      @cores ||= Concurrent.processor_count
    end

    def giza_pp_path
      'executables/giza-pp'
    end

    def source_language
      source_config.fetch('language')
    end

    def target_language
      target_config.fetch('language')
    end

    def source_config
      @source_config ||= params.fetch('source')
    end

    def target_config
      @target_config ||= params.fetch('target')
    end

    def data_root_path
      "data/#{config_label}"
    end

    def truecast_file_base_path
      "#{file_base_path}.true"
    end

    def truecase_model_base_path
      "#{file_base_path}.truecase-model"
    end

    def tokenized_file_base_path
      "#{file_base_path}.tok"
    end

    def corpus_path
      "#{data_root_path}/corpus"
    end

    def cleaned_corpora_file_path
      "#{file_base_path}.clean"
    end

    %i[
      corpora_base_name
      corpora_download_url
    ].each do |attribute|
      define_method(attribute) do
        params.fetch(attribute.to_s)
      end
    end

    def max_lines_to_use
      params['max_lines_to_use']
    end

    def download_test_data?
      !params.key?('test_data_percents')
    end

    def exist?
      root_params.key?(config_label)
    end

    private

    def file_base_path
      "#{corpus_path}/#{corpora_base_name}"
    end

    def params
      raise NotImplementedError
    end
  end
end
