class Moses
  class Trainer
    attr_reader :config, :prebinarized_language_model_path, :binarized_language_model_path

    def initialize(config)
      @config = config

      @prebinarized_language_model_path = \
        "#{config.prebinarized_language_model_base_path}.#{config.target_language}"
      @binarized_language_model_path = \
        "#{config.binarized_language_model_base_path}.#{config.target_language}"

    end

    def call
      train_language_model
      binarize_language_model
      train_translation_system
    end

    private

    def train_translation_system
      GeneratedFile.new(
        config.trained_model_config_path,
        command: "
          #{Moses::MOSES_PATH}/scripts/training/train-model.perl -root-dir #{config.training_path} \
          -cores #{config.cores} \
          -corpus #{config.cleaned_corpora_file_path} \
          -f #{config.source_language} -e #{config.target_language} \
          -alignment grow-diag-final-and -reordering msd-bidirectional-fe \
          -lm 0:3:#{Dir.pwd}/#{binarized_language_model_path}:8 \
          -external-bin-dir #{Moses::MOSES_PATH}/tools \
      ").create
    end

    def train_language_model
      GeneratedFile.new(
        prebinarized_language_model_path,
        command: "
          ./#{Moses::MOSES_PATH}/bin/lmplz -o 3 <#{Dir.pwd}/#{config.truecast_file_base_path}.#{config.target_language} \
            > #{prebinarized_language_model_path}
        "
      ).create
    end

    def binarize_language_model
      GeneratedFile.new(
        binarized_language_model_path,
        command: "
          ./#{Moses::MOSES_PATH}/bin/build_binary \
            #{prebinarized_language_model_path} \
            #{binarized_language_model_path}
        "
      ).create
    end
  end
end
