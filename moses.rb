class Moses
  require 'active_support/core_ext/module/delegation'
  require 'active_support/core_ext/hash/slice'

  require_relative 'moses/installer'
  require_relative 'moses/data_formatter'
  require_relative 'moses/trainer'
  require_relative 'moses/tuner'
  require_relative 'moses/data_fetcher'
  require_relative 'moses/logger'
  require_relative 'moses/command_runner'
  require_relative 'moses/generated_file'
  require_relative 'moses/training/config'

  MOSES_PATH = 'executables/mosesdecoder'.freeze

  attr_reader :args, :config

  def initialize(args)
    @args = args
    @config = Training::Config.new(args.first)
  end

  def self.translate(config_name, foreign_text)
    config = Training::Config.new(config_name)
    `echo "#{foreign_text}" | #{Moses::MOSES_PATH}/bin/moses -f #{config.translation_system_path}/mert-work/moses.ini`
  end

  def call
    Moses::Installer.new(config).call
    Moses::DataFormatter.new(config).call
    Moses::Trainer.new(config).call
    Moses::Tuner.new(config).call
  end
end
